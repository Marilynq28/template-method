using System;
using System.Collections.Generic;
using System.Text;

namespace patrontemplate
{
    interface IPrimitiva
    {
        //creamos dos metodos 
        double Decorar(int cantidad);
        double Empacar(int cantidad);
    }
}
