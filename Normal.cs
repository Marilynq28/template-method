using System;
using System.Collections.Generic;
using System.Text;

namespace patrontemplate
{
    class Normal : IPrimitiva
    {
        public double Decorar(int cantidad)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Hacer {0} veces", cantidad);
            Console.WriteLine("Pintar logotipo");
            Console.WriteLine("Poner Brillos");
            Console.WriteLine("Adicionar cromos");

            return 7.50 * cantidad; 
        }

        public double Empacar(int cantidad)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Hacer {0} veces", cantidad);
            Console.WriteLine("Preparar caja");
            Console.WriteLine("Poner Instrutivo");
            Console.WriteLine("Poner garantia");
            Console.WriteLine("Cellar caja");

            return 12.50 * cantidad;
        }
    }
}
